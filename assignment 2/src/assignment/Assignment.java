/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

/**
 *
 * @author Rahul
 */
import java.util.ArrayList;
import java.util.Scanner;

//Class that contains details of every user
class User{
    private String name, city, country;
    private int gender, dedicatedBricks = 0,admin;
    User(String name, int gender, String city, String country,int admin){
        this.name = name;
        this.gender = gender;
        this.city = city;
        this.country = country;
        this.admin=admin;
        this.dedicatedBricks = dedicatedBricks;
    }
    String getName(){
        return name;
    } 
    int getGender(){
        return gender;
    }
    int getadmin(){
        return admin;
    }
    String getCity(){
        return city;
    }
    String getCountry(){
        return country;
    }
    int getDedicatedBricks(){
        return dedicatedBricks;
    }
    void setDedicatedBricks(int dedicatedBricks){
        this.dedicatedBricks = dedicatedBricks;
    }
}

//class that contains the details of the wall
class Wall{
    private int numWalls = 0, numBricks = 0;
    //a new brick is added (a wall can have max 90 bricks)
    void brickAdded(){
        if(numBricks < 90)
            numBricks++;
        else{
            numWalls++;
            numBricks = 1;
        }
    }
    int getWalls(){
        return numWalls;
    }
    int getBricks(){
        return (numWalls - 1) * 90 + numBricks;
    }
}

//class that contains the details of the brick
class Brick{
    private User owner, dedicatedUser,admin;
    private String color;
    Brick(User owner, User dedicatedUser, String color){
        this.owner = owner;
        this.dedicatedUser = dedicatedUser;
        this.color = color;
        this.admin = admin;
    }
    User getOwner(){
        return owner;
    }
  //  User getadmin(){
    //    return admin;
    //}
    User getDedicatedUser(){
        return dedicatedUser;
    }
    String getColor(){
        return color;
    }
    void setDedicatedUser(User dedicatedUser){
        this.dedicatedUser = dedicatedUser;
    }
    void setColor(String color){
        this.color = color;
    }
}

public class Assignment {
    public static void main(String[] args) {
        String[] userDetails;
        int gender,admin;
        ArrayList<User> usersList = new ArrayList<>(); 
        User ob;
        System.out.println("Enter details of 5 users: Name Gender City Country and whether he or she is admin or not");
        System.out.println("Name Gender City Country Admin");
        Scanner sc = new Scanner(System.in);
        try{
            for(int i=0; i<5; i++){
                userDetails = sc.nextLine().split(" ");
                //each User class object represents a user
                if(userDetails[1].toUpperCase().equals("MALE") && userDetails[4].toUpperCase().equals("YES"))
                    ob = new User(userDetails[0], 0, userDetails[2], userDetails[3],0);
                else
                    ob = new User(userDetails[0], 1, userDetails[2], userDetails[3],1);
                //add user object to a list
                usersList.add(ob);
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
       Assignment object = new Assignment();
        object.displayUsers(usersList);
        object.displayHottest(usersList);
    }

    //display the users
    private void displayUsers(ArrayList<User> usersList){
        User ob;
        System.out.println("Users List");
        System.out.println("Id \t Name \t Gender \t City \t Country \t Admin");
        for(int i=0; i<usersList.size(); i++){
            ob = usersList.get(i);
            System.out.println((i+1) + "\t" +ob.getName() + "\t" + ob.getGender() + "\t" + ob.getCity() + "\t" + ob.getCountry() +"\t"+ ob.getadmin());
        }
        System.out.println("Choose user id");
        Scanner sc = new Scanner(System.in);
        int userId = sc.nextInt();
        userActions(usersList, userId-1);
    }
    
    //ask user to perform some action
    private void userActions(ArrayList<User> usersList, int userId)
    {
       
        int s=usersList.get(userId).getadmin();
        System.out.println("Hello " + usersList.get(userId).getName());
        if(s==0)
            System.out.println(":User is admin:");
        else
            System.out.println(":User is not admin:");
        System.out.println("What you want to do?");
        System.out.println("1. Paint a new brick");
       // if(s==0){
         //    System.out.println("1. Paint a new brick");
             //System.out.println("2.Watch a Brick");
       // }
        //else
          //  System.out.println("1.Watch a Brick");
        Scanner sc = new Scanner(System.in);
        int op = sc.nextInt();
        if(op == 1)
            paintBrick(usersList, usersList.get(userId));
       // else
         //   displayBricks(bricksList);
            
    }
    
    //paint a new brick in a wall of a city in a country
    private void paintBrick(ArrayList<User> usersList, User user){
        System.out.println(user.getName() + " wants to paint a brick");
        System.out.println("Enter color for your brick");
        Scanner sc = new Scanner(System.in);
        String color = sc.next();
        System.out.println("To whom you want to dedicate this brick?");
        User ob;
        System.out.println("Id \t Name \t Gender \t City \t Country");
        for(int i=0; i<usersList.size(); i++){
            ob = usersList.get(i);
            System.out.println((i+1) + "\t" +ob.getName() + "\t" + ob.getGender() + "\t" + ob.getCity() + "\t" + ob.getCountry());
        }
        System.out.println("Enter user id to dedicate");
        int userId = sc.nextInt();
        User dedicatedUser = usersList.get(userId-1);
        //create an object of Brick class
        Brick brick = new Brick(user, dedicatedUser, color);
        //add all bricks to a list
        ArrayList<Brick> bricksList = new ArrayList<>();
        bricksList.add(brick);
        //increment the dedicatedBricks of dedicated user
        dedicatedUser.setDedicatedBricks(dedicatedUser.getDedicatedBricks() + 1);  
        //add brick to wall
        new Wall().brickAdded();
        displayBricks(bricksList);
    }
    
    //display all bricks
    private void displayBricks(ArrayList<Brick> bricksList){
        Brick brick;
        System.out.println("Owner \t Paint \t Dedicated To");
        for(int i=0; i<bricksList.size(); i++){
            brick = bricksList.get(i);
            System.out.println(brick.getOwner().getName() + "\t" + brick.getColor() + "\t" + brick.getDedicatedUser().getName());
        }
    }
    
    private void displayHottest(ArrayList<User> usersList){
        System.out.println("See who is hottest?");
        System.out.println("1. In Boys");
        System.out.println("2. In Girls");
        System.out.println("3. Not Interested");
        Scanner sc = new Scanner(System.in);
        int op = sc.nextInt();
        if(op == 1)
            hottestBoy(usersList);
        else if(op == 2)
            hottestGirl(usersList);
        else 
            return;
    }
    
    //get the hottest boy with max. dedicated bricks
    private void hottestBoy(ArrayList<User> usersList){
        User user, hottest = null;
        for(int i=0; i<usersList.size(); i++){
            user = usersList.get(i);
            if(user.getGender() == 0  && (hottest == null || hottest.getDedicatedBricks() < user.getDedicatedBricks())){
                hottest = user;
            }
        }
        System.out.println("Hottest Boy is " + hottest.getName());
    }
    
    //get the hottest girl with max. dedicated bricks
    private void hottestGirl(ArrayList<User> usersList){
        User user, hottest = null;
        for(int i=0; i<usersList.size(); i++){
            user = usersList.get(i);
            if(user.getGender() == 1  && (hottest == null || hottest.getDedicatedBricks() < user.getDedicatedBricks())){
                hottest = user;
            }
        }
         System.out.println("Hottest Girl is " + hottest.getName());
    }
}


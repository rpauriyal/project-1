/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package craterzone;

/**
 *
 * @author Rahul
 */
public class App {

    /**
     * @param args the command line arguments
     */
  public static void main(String[] args) {
       Employee e1=new Employee("Rahul");
       Employee e2=new Employee("Vijay");
       Employee e3=new Employee("Dinesh");
       Employee e4=new Employee("Puru");
       
        Item i1=new Item("Onion",30,2);
       Item i2=new Item("Tomato",40,5);
       Item i3=new Item("Potato",35,7);
       Item i4=new Item("Brinjal",20);
       
       DiscountBill d1=new DiscountBill(e1,true);
       DiscountBill d2=new DiscountBill(e2,true);
       DiscountBill d3=new DiscountBill(e3,true);
       DiscountBill d4=new DiscountBill(e4,false);
       
       d1.add(i1);
       d1.add(i3);
       
       d2.add(i2);
      
       d3.add(i1);
       d3.add(i2);
       d3.add(i3);
       
       d4.add(i4);
     System.out.println("Is eligible for Discount ::  ");
     System.out.println(e1+"  "+d1.checkDis());
     System.out.println(e2+"  "+d2.checkDis());
     System.out.println(e3+"  "+d3.checkDis());
     System.out.println(e4+"  "+d4.checkDis());
     
     System.out.println("\nList of item of ::"+ e1);
     d1.printReceipt();
     System.out.println("Discount Amount is :"+d1.getDiscountAmount());
     System.out.printf("Discount Percent is :%.2f  \n",(float)d1.getDiscountPercent());
     System.out.println("Total Amount is :"+d1.getTotal());
     
      System.out.println("\nList of item of :: "+ e2);
     d2.printReceipt();
     System.out.println("Discount Amount is : "+d2.getDiscountAmount());
     System.out.printf("Discount Percent is :%.2f  \n",(float)d2.getDiscountPercent());
     System.out.println("Total Amount is "+d2.getTotal());
     
     System.out.println("\nList of item of ::"+ e3);
     d3.printReceipt();
     System.out.println("Discount Amount is  ::"+d3.getDiscountAmount());
     System.out.printf("Discount Percent is : %.2f \n ",(float)d3.getDiscountPercent());
     System.out.println("Total Amount is :"+d3.getTotal());
     
      System.out.println("\nList of item of ::"+ e4);
     d4.printReceipt();
     System.out.println("Discount Amount is  :"+d4.getDiscountAmount());
     System.out.printf("Discount Percent is :%.2f \n ",(float)d4.getDiscountPercent());
     System.out.println("Total Amount is :"+d4.getTotal());
     
     System.out.println("\nNumber of employee who were eligible for discount ::"+DiscountBill.getDiscountCount());
    // System.out.println(d2.getDiscountAmount());
     //System.out.println(d3.getDiscountAmount());
      
/*   System.out.println("\t......Menu.......");
   System.out.println("1.Create Employee");
   System.out.println("2.Add Item");
   System.out.println("3.Show Discount Amount");
   System.out.println("4.Show Discount percent");
   System.out.println("5.Total bill");
*/ 
}
    
    
}


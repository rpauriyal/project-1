/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package craterzone;

/**
 *
 * @author Rahul
 */
public class Item {
    public double price;
    public double discount;
    public String itemname;
    
    public Item(String n,double price,double discount)
    {
        itemname=n;
        this.price=price;
        this.discount=discount;
    }
    public Item(String n,double price)
    {
        itemname=n;
        this.price=price;
       
    }
    
    public double getPrice()//return the price of the item
    {
        return price;
    }
    
    public double getDiscount()//returns the discount for this item
    {
        return discount;
    }
    
    public String toString()
    {
        return itemname;
    }
}

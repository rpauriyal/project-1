/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package craterzone;

/**
 *
 * @author Rahul
 */
public class DiscountBill extends VegetableBill {
   boolean  preferred;
   static int count; 
   double  d;
   double damt=0;
    public DiscountBill(Employee clerk,boolean preferred)
    {
       super(clerk);
      this.preferred=preferred; 
        d=super.dis;
    }
    public String checkDis()
    {
        if(preferred==true)
        {  
            count++;
            return "Yes";
        }
        else
            return "No";
    
    }
   public static int getDiscountCount()
    {
        return count;
        
    }
    public double getDiscountAmount()
    {
        return dis;
    }
    public double getTotal()
    {
        return bill-dis;
    }
    public double getDiscountPercent()
    {
            if(dis==0.0)
                return 0;
            else
                return (dis/bill)*100;
    }
    

}